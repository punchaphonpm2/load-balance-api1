require('dotenv').config();

module.exports =  {
    PORT:process.env.PORT,
    NODE_ENV: process.env.NODE_ENV,
    MYSQL_HOST: process.env.MYSQL_HOST,
    MYSQL_USERNAME: process.env.MYSQL_USERNAME,
    MYSQL_PASSWORD: process.env.MYSQL_PASSWORD,
    JWT_SECRET:process.env.JWT_SECRET,
    DOMAIN: process.env.DOMAIN,
}