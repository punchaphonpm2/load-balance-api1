const express = require("express");
const path = require("path");
const cookieParser = require("cookie-parser");
const logger = require("morgan");
const cors = require("cors");

const passport = require("passport");

const app = express();

app.get('/', (req, res) => {
  console.log("connect api 1");
  res.send('test api 1')
})

app.use(cors());

app.io = require("socket.io")();
global.io = app.io;

app.use(logger("dev"));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, "public")));
app.use(passport.initialize());

app.io.on("connection", function (socket) {
  console.log("socketio user connected...", socket.id);
});

module.exports = app;
